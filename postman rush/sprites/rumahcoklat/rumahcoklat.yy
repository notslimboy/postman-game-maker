{
    "id": "a9ee97b0-b6af-460d-8a23-3ce09c2eb9c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rumahcoklat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c09d1949-0bd6-438d-89cd-afdc16b8d070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9ee97b0-b6af-460d-8a23-3ce09c2eb9c1",
            "compositeImage": {
                "id": "15c798b5-7df1-4816-bf2b-452098412fac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c09d1949-0bd6-438d-89cd-afdc16b8d070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cacb806-64cd-49a4-bcbf-163aaa8b64e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c09d1949-0bd6-438d-89cd-afdc16b8d070",
                    "LayerId": "229cbfcb-86b3-4706-9d67-710c2bb49be1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "229cbfcb-86b3-4706-9d67-710c2bb49be1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9ee97b0-b6af-460d-8a23-3ce09c2eb9c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}