{
    "id": "aeb59ce6-111c-4ffd-8da9-305649513c42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "kiri",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "950419d7-2d26-4ea4-bb8b-767627986ff8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeb59ce6-111c-4ffd-8da9-305649513c42",
            "compositeImage": {
                "id": "5c64f5e4-bb0e-4d59-bdc3-773452fcc51a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "950419d7-2d26-4ea4-bb8b-767627986ff8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d1e0f40-b72c-4c5f-8426-918f6cafdabd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "950419d7-2d26-4ea4-bb8b-767627986ff8",
                    "LayerId": "798cce3c-f9c3-49c4-b5ee-84b3349e78ec"
                }
            ]
        },
        {
            "id": "5ee5ec86-0fdc-4482-ac64-797ee3498740",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeb59ce6-111c-4ffd-8da9-305649513c42",
            "compositeImage": {
                "id": "77d93167-7155-43ec-9c89-be95fe5ec83d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee5ec86-0fdc-4482-ac64-797ee3498740",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead9f14e-e561-472e-95e8-8a0dfba4c2ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee5ec86-0fdc-4482-ac64-797ee3498740",
                    "LayerId": "798cce3c-f9c3-49c4-b5ee-84b3349e78ec"
                }
            ]
        },
        {
            "id": "96af96bd-fe9d-4d52-8ab2-1ae0f158fd5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeb59ce6-111c-4ffd-8da9-305649513c42",
            "compositeImage": {
                "id": "7fab7ee5-4a1c-49b9-b4a9-f0b32b893593",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96af96bd-fe9d-4d52-8ab2-1ae0f158fd5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54c82933-b7f4-4a74-a5ea-5e47a0bd0298",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96af96bd-fe9d-4d52-8ab2-1ae0f158fd5e",
                    "LayerId": "798cce3c-f9c3-49c4-b5ee-84b3349e78ec"
                }
            ]
        },
        {
            "id": "0acb6f98-a0e3-4126-8b87-db1f5d5bc44a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeb59ce6-111c-4ffd-8da9-305649513c42",
            "compositeImage": {
                "id": "d0f47b82-9cbb-4006-b81f-9c0325ff1d8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0acb6f98-a0e3-4126-8b87-db1f5d5bc44a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7deb6621-23b4-49cc-a6f8-24a914f5a174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0acb6f98-a0e3-4126-8b87-db1f5d5bc44a",
                    "LayerId": "798cce3c-f9c3-49c4-b5ee-84b3349e78ec"
                }
            ]
        },
        {
            "id": "7d0f1427-e443-4b39-aac2-f452fc8ad749",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeb59ce6-111c-4ffd-8da9-305649513c42",
            "compositeImage": {
                "id": "314f5029-bd65-453b-9f54-07eeeb87ba1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d0f1427-e443-4b39-aac2-f452fc8ad749",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63593e6e-f981-416a-98d2-58ddb5a5ffea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d0f1427-e443-4b39-aac2-f452fc8ad749",
                    "LayerId": "798cce3c-f9c3-49c4-b5ee-84b3349e78ec"
                }
            ]
        },
        {
            "id": "14b86f85-492c-42b0-9cfc-aca9abf0b909",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeb59ce6-111c-4ffd-8da9-305649513c42",
            "compositeImage": {
                "id": "0319ae8f-6c67-4d7e-976c-73040f0cb55d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14b86f85-492c-42b0-9cfc-aca9abf0b909",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ecaff28-4fff-4098-8da4-b77453274cb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14b86f85-492c-42b0-9cfc-aca9abf0b909",
                    "LayerId": "798cce3c-f9c3-49c4-b5ee-84b3349e78ec"
                }
            ]
        },
        {
            "id": "d946b557-e640-4891-b96f-da1a38eefc38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeb59ce6-111c-4ffd-8da9-305649513c42",
            "compositeImage": {
                "id": "4557435b-c052-4df1-967b-58db3fa9e543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d946b557-e640-4891-b96f-da1a38eefc38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec8c6471-f6ca-40f9-84ab-75fbc7de77d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d946b557-e640-4891-b96f-da1a38eefc38",
                    "LayerId": "798cce3c-f9c3-49c4-b5ee-84b3349e78ec"
                }
            ]
        },
        {
            "id": "eeb4ee20-74cf-4c7f-be1f-4b3895a7720b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeb59ce6-111c-4ffd-8da9-305649513c42",
            "compositeImage": {
                "id": "a42c0381-7a47-4fa1-a743-cb3372087a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeb4ee20-74cf-4c7f-be1f-4b3895a7720b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d176bc0-6a23-418d-97c2-3b45f5d0e48f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeb4ee20-74cf-4c7f-be1f-4b3895a7720b",
                    "LayerId": "798cce3c-f9c3-49c4-b5ee-84b3349e78ec"
                }
            ]
        },
        {
            "id": "cd15dc3a-6d40-4720-8170-f2958075cb50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeb59ce6-111c-4ffd-8da9-305649513c42",
            "compositeImage": {
                "id": "3e444cb7-254a-4252-a2cd-308a7975c61e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd15dc3a-6d40-4720-8170-f2958075cb50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "671bc4e4-49af-4283-bc69-8196f18fd580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd15dc3a-6d40-4720-8170-f2958075cb50",
                    "LayerId": "798cce3c-f9c3-49c4-b5ee-84b3349e78ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "798cce3c-f9c3-49c4-b5ee-84b3349e78ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aeb59ce6-111c-4ffd-8da9-305649513c42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 26
}