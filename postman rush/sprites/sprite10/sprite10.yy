{
    "id": "917724ec-2f9f-4027-8ebe-92f14afc8375",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d489dd7-8dfc-441a-a42e-caffd394a617",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "917724ec-2f9f-4027-8ebe-92f14afc8375",
            "compositeImage": {
                "id": "2d852442-d3e0-477a-a8a4-463565ce3824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d489dd7-8dfc-441a-a42e-caffd394a617",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "806e7d2b-7088-452d-92af-00b7e16bdd70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d489dd7-8dfc-441a-a42e-caffd394a617",
                    "LayerId": "b0dcab3d-4cf8-4f30-8bd5-30a859aeb354"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "b0dcab3d-4cf8-4f30-8bd5-30a859aeb354",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "917724ec-2f9f-4027-8ebe-92f14afc8375",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}