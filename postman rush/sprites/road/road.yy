{
    "id": "c566db52-2157-40af-ade4-6fdfc054e78c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "road",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8680b70-8675-4903-85c8-c8a6e4557825",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c566db52-2157-40af-ade4-6fdfc054e78c",
            "compositeImage": {
                "id": "fc78d943-cbe8-4af0-9e83-73032c73d3f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8680b70-8675-4903-85c8-c8a6e4557825",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "297f0ded-55ef-42a4-b6c2-78a3fefd6e05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8680b70-8675-4903-85c8-c8a6e4557825",
                    "LayerId": "3c050cab-0333-433e-b8ac-a29a6689e514"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "3c050cab-0333-433e-b8ac-a29a6689e514",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c566db52-2157-40af-ade4-6fdfc054e78c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}