{
    "id": "b84b99cf-ea33-46e7-9ae3-f44261963e0d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd541d80-5cd0-4b88-8ac7-ed76418459ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b84b99cf-ea33-46e7-9ae3-f44261963e0d",
            "compositeImage": {
                "id": "a9b7cd7d-abaf-426f-b59d-82da1c02465a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd541d80-5cd0-4b88-8ac7-ed76418459ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aebc7861-0cf1-407e-b679-5e4ce7fb804a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd541d80-5cd0-4b88-8ac7-ed76418459ee",
                    "LayerId": "a9ad28e6-348f-461b-8482-f9017d0af25f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a9ad28e6-348f-461b-8482-f9017d0af25f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b84b99cf-ea33-46e7-9ae3-f44261963e0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}