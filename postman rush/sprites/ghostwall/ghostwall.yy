{
    "id": "5539e672-3722-421d-94ed-6dc9afba962d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ghostwall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb9ceeb6-626d-4512-8624-19adfab79c66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5539e672-3722-421d-94ed-6dc9afba962d",
            "compositeImage": {
                "id": "3e5c9bd5-f65a-4c26-83b5-ef32fd9f1bd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb9ceeb6-626d-4512-8624-19adfab79c66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d93f82d5-5069-4a9d-8a06-03747f8fd3fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb9ceeb6-626d-4512-8624-19adfab79c66",
                    "LayerId": "2e122fd5-aed3-49a7-bb19-c61c9725878d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2e122fd5-aed3-49a7-bb19-c61c9725878d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5539e672-3722-421d-94ed-6dc9afba962d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}