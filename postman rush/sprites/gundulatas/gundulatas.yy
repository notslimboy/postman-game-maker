{
    "id": "9422ecdf-42ed-4e42-bdc1-ef83dbc22444",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gundulatas",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7a2eaf6-ee3b-4f33-b8b6-b80a52fff3dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422ecdf-42ed-4e42-bdc1-ef83dbc22444",
            "compositeImage": {
                "id": "55f45c67-4fa7-4f79-a01b-2caecb7f7f16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7a2eaf6-ee3b-4f33-b8b6-b80a52fff3dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f0f833f-cbcc-484f-ad5f-b451e1096025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7a2eaf6-ee3b-4f33-b8b6-b80a52fff3dc",
                    "LayerId": "48b01c6d-d2dd-43dd-9ec2-820761c6aea9"
                }
            ]
        },
        {
            "id": "2dda63ed-7b56-4b1f-aaba-3579cc8a03c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422ecdf-42ed-4e42-bdc1-ef83dbc22444",
            "compositeImage": {
                "id": "c6df31ef-30ae-42a5-8bee-95623a73a382",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dda63ed-7b56-4b1f-aaba-3579cc8a03c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2be336bc-e4b1-493b-836d-5b6eadd80ed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dda63ed-7b56-4b1f-aaba-3579cc8a03c9",
                    "LayerId": "48b01c6d-d2dd-43dd-9ec2-820761c6aea9"
                }
            ]
        },
        {
            "id": "d227861d-b073-4c89-b400-f664ef901de9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422ecdf-42ed-4e42-bdc1-ef83dbc22444",
            "compositeImage": {
                "id": "0f55c8de-8431-467b-903a-e8f51deda9e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d227861d-b073-4c89-b400-f664ef901de9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5900c02f-2275-4109-b3df-56f8a6f19e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d227861d-b073-4c89-b400-f664ef901de9",
                    "LayerId": "48b01c6d-d2dd-43dd-9ec2-820761c6aea9"
                }
            ]
        },
        {
            "id": "4a2ba7b4-48cc-4934-9761-13a0606cdaf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422ecdf-42ed-4e42-bdc1-ef83dbc22444",
            "compositeImage": {
                "id": "0c2e1ec7-1299-4d42-8d2f-e0643031e5d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a2ba7b4-48cc-4934-9761-13a0606cdaf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a6ba68-1e24-4869-94e0-d99f215eb6a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a2ba7b4-48cc-4934-9761-13a0606cdaf7",
                    "LayerId": "48b01c6d-d2dd-43dd-9ec2-820761c6aea9"
                }
            ]
        },
        {
            "id": "3087d4fd-ba97-40ed-b599-357c967a6498",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422ecdf-42ed-4e42-bdc1-ef83dbc22444",
            "compositeImage": {
                "id": "ff41b484-3918-4754-aea4-d76ba2f3b18f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3087d4fd-ba97-40ed-b599-357c967a6498",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75736ef3-8c57-423f-a8de-35c5b99b996a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3087d4fd-ba97-40ed-b599-357c967a6498",
                    "LayerId": "48b01c6d-d2dd-43dd-9ec2-820761c6aea9"
                }
            ]
        },
        {
            "id": "0626d15b-4656-45ee-8bbe-23ddc86d0256",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422ecdf-42ed-4e42-bdc1-ef83dbc22444",
            "compositeImage": {
                "id": "783ef688-4f9a-431a-a141-4022d5a4b06e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0626d15b-4656-45ee-8bbe-23ddc86d0256",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9080d443-9037-498d-837f-f27c81dddab4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0626d15b-4656-45ee-8bbe-23ddc86d0256",
                    "LayerId": "48b01c6d-d2dd-43dd-9ec2-820761c6aea9"
                }
            ]
        },
        {
            "id": "7b90c768-fa46-4d44-8d20-e7ab98fb9d25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422ecdf-42ed-4e42-bdc1-ef83dbc22444",
            "compositeImage": {
                "id": "45be0b89-0112-421a-b45d-626f30c24e64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b90c768-fa46-4d44-8d20-e7ab98fb9d25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a411d08-1a1c-4a30-9688-f9a527f179b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b90c768-fa46-4d44-8d20-e7ab98fb9d25",
                    "LayerId": "48b01c6d-d2dd-43dd-9ec2-820761c6aea9"
                }
            ]
        },
        {
            "id": "9488dea8-80ef-4061-99d9-078e3a7a8e20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422ecdf-42ed-4e42-bdc1-ef83dbc22444",
            "compositeImage": {
                "id": "a60cc852-7315-4750-b52f-e98aad5b9a60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9488dea8-80ef-4061-99d9-078e3a7a8e20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "254457c5-7e6e-45d8-b174-ee0a10f52757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9488dea8-80ef-4061-99d9-078e3a7a8e20",
                    "LayerId": "48b01c6d-d2dd-43dd-9ec2-820761c6aea9"
                }
            ]
        },
        {
            "id": "6ec1a2cc-47e1-48ed-9ee9-d462ef0a57a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9422ecdf-42ed-4e42-bdc1-ef83dbc22444",
            "compositeImage": {
                "id": "87ae009a-2ec0-4c54-bdec-344d9ea9c40e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ec1a2cc-47e1-48ed-9ee9-d462ef0a57a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aadc9e8c-e727-4039-aa4c-6c2f88504566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ec1a2cc-47e1-48ed-9ee9-d462ef0a57a5",
                    "LayerId": "48b01c6d-d2dd-43dd-9ec2-820761c6aea9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "48b01c6d-d2dd-43dd-9ec2-820761c6aea9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9422ecdf-42ed-4e42-bdc1-ef83dbc22444",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 17,
    "yorig": 23
}