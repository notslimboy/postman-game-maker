{
    "id": "93163ae0-6142-4633-9139-818f11a9ba2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gundulkanan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53a60ece-e2b9-4bd8-98fb-e59874e44c45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93163ae0-6142-4633-9139-818f11a9ba2a",
            "compositeImage": {
                "id": "1cf1dee2-92a7-4820-bbc9-2caba47eb07e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53a60ece-e2b9-4bd8-98fb-e59874e44c45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d86d483e-9ddb-40ba-82cb-bc77b0b138d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53a60ece-e2b9-4bd8-98fb-e59874e44c45",
                    "LayerId": "342faa33-b2d6-4348-a2a9-b63ceace9ef7"
                }
            ]
        },
        {
            "id": "282f23f1-4317-4c55-9367-4623f22967ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93163ae0-6142-4633-9139-818f11a9ba2a",
            "compositeImage": {
                "id": "fba94fc6-a945-46d2-9a7f-bfec8e7e025a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "282f23f1-4317-4c55-9367-4623f22967ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e685d0c8-bd50-4207-8cb4-c14e355e6c51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "282f23f1-4317-4c55-9367-4623f22967ad",
                    "LayerId": "342faa33-b2d6-4348-a2a9-b63ceace9ef7"
                }
            ]
        },
        {
            "id": "0325ff08-b671-407e-b9f6-7ec855f9b582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93163ae0-6142-4633-9139-818f11a9ba2a",
            "compositeImage": {
                "id": "5897daf8-0e6b-4267-916f-f542be9f1ae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0325ff08-b671-407e-b9f6-7ec855f9b582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8c88c7b-bee2-4d25-bbbc-641b045b0e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0325ff08-b671-407e-b9f6-7ec855f9b582",
                    "LayerId": "342faa33-b2d6-4348-a2a9-b63ceace9ef7"
                }
            ]
        },
        {
            "id": "850082ae-0766-4f1e-9a5d-181104612062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93163ae0-6142-4633-9139-818f11a9ba2a",
            "compositeImage": {
                "id": "0a15e7b8-ef13-4a9c-8ee0-8a49212023f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "850082ae-0766-4f1e-9a5d-181104612062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7f14a24-df99-4902-ae35-f4d3115d5074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "850082ae-0766-4f1e-9a5d-181104612062",
                    "LayerId": "342faa33-b2d6-4348-a2a9-b63ceace9ef7"
                }
            ]
        },
        {
            "id": "a256a6a1-c470-47b4-a410-6db458eb59ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93163ae0-6142-4633-9139-818f11a9ba2a",
            "compositeImage": {
                "id": "3a0fb680-6c46-4550-91c5-f13351487d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a256a6a1-c470-47b4-a410-6db458eb59ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e30a20f-0dd8-48da-aa39-a6dc45491312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a256a6a1-c470-47b4-a410-6db458eb59ea",
                    "LayerId": "342faa33-b2d6-4348-a2a9-b63ceace9ef7"
                }
            ]
        },
        {
            "id": "d80db79b-5783-4099-add8-7f0c7db2d27f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93163ae0-6142-4633-9139-818f11a9ba2a",
            "compositeImage": {
                "id": "bab7ed86-7311-4650-8a9c-6b5702ca6c1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d80db79b-5783-4099-add8-7f0c7db2d27f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b841505-663a-447d-bef7-b6d4c49cd0a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d80db79b-5783-4099-add8-7f0c7db2d27f",
                    "LayerId": "342faa33-b2d6-4348-a2a9-b63ceace9ef7"
                }
            ]
        },
        {
            "id": "fe2ce703-754d-4e67-9789-e81f5598e29e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93163ae0-6142-4633-9139-818f11a9ba2a",
            "compositeImage": {
                "id": "3a0fd2eb-6ab6-4198-8f44-1b41eb878412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe2ce703-754d-4e67-9789-e81f5598e29e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "768c0cce-e901-40c9-a257-d43e062a54a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe2ce703-754d-4e67-9789-e81f5598e29e",
                    "LayerId": "342faa33-b2d6-4348-a2a9-b63ceace9ef7"
                }
            ]
        },
        {
            "id": "58cf4b7b-7701-472c-8265-8f31d08f4bde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93163ae0-6142-4633-9139-818f11a9ba2a",
            "compositeImage": {
                "id": "19737bf6-6901-4c7b-8cd4-0c7950d7c824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58cf4b7b-7701-472c-8265-8f31d08f4bde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d11c1a20-1428-404a-8762-277e412457ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58cf4b7b-7701-472c-8265-8f31d08f4bde",
                    "LayerId": "342faa33-b2d6-4348-a2a9-b63ceace9ef7"
                }
            ]
        },
        {
            "id": "cc8ea839-97b9-492c-b32a-9f9845b4f263",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93163ae0-6142-4633-9139-818f11a9ba2a",
            "compositeImage": {
                "id": "ee784cb9-f807-4def-8012-5b11bb4f237f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc8ea839-97b9-492c-b32a-9f9845b4f263",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a287be50-3fd3-4f4e-92df-5ef36441e4fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc8ea839-97b9-492c-b32a-9f9845b4f263",
                    "LayerId": "342faa33-b2d6-4348-a2a9-b63ceace9ef7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "342faa33-b2d6-4348-a2a9-b63ceace9ef7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93163ae0-6142-4633-9139-818f11a9ba2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 35,
    "xorig": 17,
    "yorig": 24
}