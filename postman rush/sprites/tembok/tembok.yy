{
    "id": "a36f24e1-ca15-41a8-b7da-f8d8a1188c4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tembok",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74c680f6-dbfb-4b77-83b5-48a54d51cfa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a36f24e1-ca15-41a8-b7da-f8d8a1188c4c",
            "compositeImage": {
                "id": "cb85180e-9e45-4149-947c-06ae5381ef0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74c680f6-dbfb-4b77-83b5-48a54d51cfa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a475deed-2f76-4ca9-b241-725369710747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74c680f6-dbfb-4b77-83b5-48a54d51cfa3",
                    "LayerId": "89ab924c-593d-4881-85cd-c09517e59b0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "89ab924c-593d-4881-85cd-c09517e59b0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a36f24e1-ca15-41a8-b7da-f8d8a1188c4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}