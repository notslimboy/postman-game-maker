{
    "id": "8e331fd0-e3c5-40a0-a434-27b6b5860c89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc4d91d9-8fbd-4a85-8f06-bb900f483353",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e331fd0-e3c5-40a0-a434-27b6b5860c89",
            "compositeImage": {
                "id": "a21812ea-6139-423c-bb9a-3091478f300a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc4d91d9-8fbd-4a85-8f06-bb900f483353",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e306a18b-641b-450a-9835-edc2f6d2eef5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc4d91d9-8fbd-4a85-8f06-bb900f483353",
                    "LayerId": "5b322337-a26a-4c07-a115-50f724ce8e9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "5b322337-a26a-4c07-a115-50f724ce8e9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e331fd0-e3c5-40a0-a434-27b6b5860c89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}