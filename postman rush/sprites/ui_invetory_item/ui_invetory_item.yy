{
    "id": "30a82924-c010-4991-a39b-9429bd99be5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ui_invetory_item",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 34,
    "bbox_right": 59,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19cfcde3-af1f-44ea-91cf-9e0b99f72a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30a82924-c010-4991-a39b-9429bd99be5a",
            "compositeImage": {
                "id": "267e326a-9a1c-434f-8c69-6089eecc5e86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19cfcde3-af1f-44ea-91cf-9e0b99f72a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36d5bc2c-fb74-45b5-9890-3709afb4b269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19cfcde3-af1f-44ea-91cf-9e0b99f72a05",
                    "LayerId": "e3e26291-cb77-4127-a471-5fe587d79a09"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 64,
    "layers": [
        {
            "id": "e3e26291-cb77-4127-a471-5fe587d79a09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30a82924-c010-4991-a39b-9429bd99be5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}