{
    "id": "008ea04f-4141-4c6b-b98c-530ee979480c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gundulbawah",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 1,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4c0c959-78b4-4136-863b-ad50b2b1f5cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008ea04f-4141-4c6b-b98c-530ee979480c",
            "compositeImage": {
                "id": "cedbcfec-7e59-4cd9-a72e-9d02c368e992",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4c0c959-78b4-4136-863b-ad50b2b1f5cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b49593da-bde8-4d59-8078-2025b95112ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4c0c959-78b4-4136-863b-ad50b2b1f5cd",
                    "LayerId": "761da286-4260-4e6b-99f1-e4de80eb9d92"
                }
            ]
        },
        {
            "id": "60693fa9-e369-4291-9735-7bf155981133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008ea04f-4141-4c6b-b98c-530ee979480c",
            "compositeImage": {
                "id": "fca709f9-38ba-41a2-ba1b-f6fb7f607ab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60693fa9-e369-4291-9735-7bf155981133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ac55a58-29bc-46cc-82c0-a8f33a483919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60693fa9-e369-4291-9735-7bf155981133",
                    "LayerId": "761da286-4260-4e6b-99f1-e4de80eb9d92"
                }
            ]
        },
        {
            "id": "159d9e35-c6e3-458b-88fc-4cc19bdbfb80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008ea04f-4141-4c6b-b98c-530ee979480c",
            "compositeImage": {
                "id": "c1d25083-d165-4486-89ff-c6cecfd68344",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "159d9e35-c6e3-458b-88fc-4cc19bdbfb80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5e76f4d-bed7-44dd-880f-ae326e6c0624",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "159d9e35-c6e3-458b-88fc-4cc19bdbfb80",
                    "LayerId": "761da286-4260-4e6b-99f1-e4de80eb9d92"
                }
            ]
        },
        {
            "id": "ea7e511a-bb69-4491-b4cd-482a4539d3b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008ea04f-4141-4c6b-b98c-530ee979480c",
            "compositeImage": {
                "id": "b885a982-b535-4d0e-8d72-e107dfc686fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea7e511a-bb69-4491-b4cd-482a4539d3b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02e75550-76b4-41a1-b1af-8b2d842b70da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea7e511a-bb69-4491-b4cd-482a4539d3b3",
                    "LayerId": "761da286-4260-4e6b-99f1-e4de80eb9d92"
                }
            ]
        },
        {
            "id": "849812f9-46a8-49b3-88b4-ffebebc14584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008ea04f-4141-4c6b-b98c-530ee979480c",
            "compositeImage": {
                "id": "c458ada2-5040-474b-9af8-ef194bb1b57e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "849812f9-46a8-49b3-88b4-ffebebc14584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "938c8e4a-b166-43ca-9bf4-94db461373f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "849812f9-46a8-49b3-88b4-ffebebc14584",
                    "LayerId": "761da286-4260-4e6b-99f1-e4de80eb9d92"
                }
            ]
        },
        {
            "id": "ee03f054-324c-4bd7-9a5a-f75c025b1851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008ea04f-4141-4c6b-b98c-530ee979480c",
            "compositeImage": {
                "id": "2eb667d8-eb9f-4442-84b1-c119a7aaaaa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee03f054-324c-4bd7-9a5a-f75c025b1851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9715c24d-aff7-496f-b193-f4deb131773f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee03f054-324c-4bd7-9a5a-f75c025b1851",
                    "LayerId": "761da286-4260-4e6b-99f1-e4de80eb9d92"
                }
            ]
        },
        {
            "id": "c6f921a9-6813-49f7-91ac-3efeb7c71aff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008ea04f-4141-4c6b-b98c-530ee979480c",
            "compositeImage": {
                "id": "d2dced44-5de5-47de-b04b-6a3f389949e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6f921a9-6813-49f7-91ac-3efeb7c71aff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c413afe-bbe6-4ebd-81aa-b8044affa924",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6f921a9-6813-49f7-91ac-3efeb7c71aff",
                    "LayerId": "761da286-4260-4e6b-99f1-e4de80eb9d92"
                }
            ]
        },
        {
            "id": "8fada778-a0d0-4026-8cc6-a44f5780854b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008ea04f-4141-4c6b-b98c-530ee979480c",
            "compositeImage": {
                "id": "315a5688-5c39-4e90-ba6b-ba13084a3458",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fada778-a0d0-4026-8cc6-a44f5780854b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eb96952-0930-472f-bc74-8e370c50606f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fada778-a0d0-4026-8cc6-a44f5780854b",
                    "LayerId": "761da286-4260-4e6b-99f1-e4de80eb9d92"
                }
            ]
        },
        {
            "id": "3dda1632-892e-4ff1-be51-881604aaa785",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008ea04f-4141-4c6b-b98c-530ee979480c",
            "compositeImage": {
                "id": "c3d5ee9a-d8b4-4749-8d30-3832fea75771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dda1632-892e-4ff1-be51-881604aaa785",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15225c47-dfc3-46b0-a92a-b06ca8faa4e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dda1632-892e-4ff1-be51-881604aaa785",
                    "LayerId": "761da286-4260-4e6b-99f1-e4de80eb9d92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "761da286-4260-4e6b-99f1-e4de80eb9d92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "008ea04f-4141-4c6b-b98c-530ee979480c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 35,
    "xorig": 17,
    "yorig": 24
}