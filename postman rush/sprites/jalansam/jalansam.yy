{
    "id": "d6f15902-37f6-4a37-93f3-4efea287df7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jalansam",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99e016cd-1d96-4f0d-8b27-1fff6beffcd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6f15902-37f6-4a37-93f3-4efea287df7c",
            "compositeImage": {
                "id": "27f467fe-4d4a-4787-9486-a4703342a8bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99e016cd-1d96-4f0d-8b27-1fff6beffcd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50d12faf-481f-4a15-8c17-7714b3a523e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99e016cd-1d96-4f0d-8b27-1fff6beffcd6",
                    "LayerId": "4f530c73-714f-46c1-b4e2-616f20ade517"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "4f530c73-714f-46c1-b4e2-616f20ade517",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6f15902-37f6-4a37-93f3-4efea287df7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}