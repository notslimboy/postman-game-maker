{
    "id": "0b9d5845-5242-4791-a9a3-dd45e5b33ef5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "resto",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 757,
    "bbox_left": 62,
    "bbox_right": 940,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ca86146-d179-4543-b47c-77fb0f9387f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9d5845-5242-4791-a9a3-dd45e5b33ef5",
            "compositeImage": {
                "id": "56ffce85-2b62-42d6-aa9a-38e78ede5822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ca86146-d179-4543-b47c-77fb0f9387f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a058847-15de-4f93-9046-bea82c4b0164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ca86146-d179-4543-b47c-77fb0f9387f8",
                    "LayerId": "5d26f299-4b31-4c43-9d9d-c80f1025f4f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 784,
    "layers": [
        {
            "id": "5d26f299-4b31-4c43-9d9d-c80f1025f4f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b9d5845-5242-4791-a9a3-dd45e5b33ef5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1003,
    "xorig": 0,
    "yorig": 0
}