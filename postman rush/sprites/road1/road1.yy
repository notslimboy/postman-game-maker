{
    "id": "8ad62134-115f-4bdc-ae48-fea25a37baba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "road1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfcee328-5732-4638-952c-3b4c5aaf04bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ad62134-115f-4bdc-ae48-fea25a37baba",
            "compositeImage": {
                "id": "d7209345-cd61-4210-9c08-4a552210aa42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfcee328-5732-4638-952c-3b4c5aaf04bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90ee05e2-4f57-4db0-90c4-a4f6b4e45955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfcee328-5732-4638-952c-3b4c5aaf04bf",
                    "LayerId": "9d3c68e9-3610-431e-a806-8bff4da744a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "9d3c68e9-3610-431e-a806-8bff4da744a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ad62134-115f-4bdc-ae48-fea25a37baba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 12
}