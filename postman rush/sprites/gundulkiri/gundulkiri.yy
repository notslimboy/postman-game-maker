{
    "id": "a2d119ee-ba1c-4f58-a724-3c32e5f0ad31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gundulkiri",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed2eb29c-be68-4128-b3d7-73662a30f2c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2d119ee-ba1c-4f58-a724-3c32e5f0ad31",
            "compositeImage": {
                "id": "309cae30-56d3-4f62-af7b-fcf91ee17705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed2eb29c-be68-4128-b3d7-73662a30f2c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ca96192-df97-4028-870a-30820c9ae675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed2eb29c-be68-4128-b3d7-73662a30f2c1",
                    "LayerId": "824273d6-fced-4660-b640-5ee6fe62f7b4"
                }
            ]
        },
        {
            "id": "f201bb03-480e-4de8-8b01-c7243554c319",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2d119ee-ba1c-4f58-a724-3c32e5f0ad31",
            "compositeImage": {
                "id": "7f16da68-d907-4c28-871d-545d2a25cee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f201bb03-480e-4de8-8b01-c7243554c319",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef33a626-fa9a-4937-bdfb-45b3d11b4c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f201bb03-480e-4de8-8b01-c7243554c319",
                    "LayerId": "824273d6-fced-4660-b640-5ee6fe62f7b4"
                }
            ]
        },
        {
            "id": "42cf2ea6-b3d3-4d8a-9511-1f0e96a7a53e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2d119ee-ba1c-4f58-a724-3c32e5f0ad31",
            "compositeImage": {
                "id": "0063a3e4-90fd-4ca6-8aab-dc3e4c0e7cbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42cf2ea6-b3d3-4d8a-9511-1f0e96a7a53e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ebb213f-466e-4820-b2dc-47ccd4098230",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42cf2ea6-b3d3-4d8a-9511-1f0e96a7a53e",
                    "LayerId": "824273d6-fced-4660-b640-5ee6fe62f7b4"
                }
            ]
        },
        {
            "id": "0805ec81-f74e-469a-821a-4786b9bfe4d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2d119ee-ba1c-4f58-a724-3c32e5f0ad31",
            "compositeImage": {
                "id": "ccf982ae-d977-4216-9ae1-9dc1beea8998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0805ec81-f74e-469a-821a-4786b9bfe4d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2a8fdb6-9e62-4b85-a5f4-8d60916d99f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0805ec81-f74e-469a-821a-4786b9bfe4d3",
                    "LayerId": "824273d6-fced-4660-b640-5ee6fe62f7b4"
                }
            ]
        },
        {
            "id": "58d3e1cc-2b49-46c9-bf95-0bfc7df0a955",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2d119ee-ba1c-4f58-a724-3c32e5f0ad31",
            "compositeImage": {
                "id": "dbaf9a2d-a5c0-4d4a-8d64-f414db2ff1a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58d3e1cc-2b49-46c9-bf95-0bfc7df0a955",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdfb2b4a-be4d-40e0-95f3-f7cb8966ba53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58d3e1cc-2b49-46c9-bf95-0bfc7df0a955",
                    "LayerId": "824273d6-fced-4660-b640-5ee6fe62f7b4"
                }
            ]
        },
        {
            "id": "ff7d6a20-85cd-45cf-bc88-8b8d3735a3c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2d119ee-ba1c-4f58-a724-3c32e5f0ad31",
            "compositeImage": {
                "id": "1c036570-a9ef-4a17-8054-9aff9b559704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff7d6a20-85cd-45cf-bc88-8b8d3735a3c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb240c83-0808-43e9-8767-88253e878d85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff7d6a20-85cd-45cf-bc88-8b8d3735a3c6",
                    "LayerId": "824273d6-fced-4660-b640-5ee6fe62f7b4"
                }
            ]
        },
        {
            "id": "47ee49a5-6bd0-4042-bcca-5ee62696eb90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2d119ee-ba1c-4f58-a724-3c32e5f0ad31",
            "compositeImage": {
                "id": "f4923a91-bb7f-48e7-b296-50ff766799f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47ee49a5-6bd0-4042-bcca-5ee62696eb90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2480f7af-9285-447b-8fb8-86be67784d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47ee49a5-6bd0-4042-bcca-5ee62696eb90",
                    "LayerId": "824273d6-fced-4660-b640-5ee6fe62f7b4"
                }
            ]
        },
        {
            "id": "132c9443-4d2d-47a1-b878-6262c09b2002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2d119ee-ba1c-4f58-a724-3c32e5f0ad31",
            "compositeImage": {
                "id": "42285d33-b23d-48c8-b0be-61aa4fc05546",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132c9443-4d2d-47a1-b878-6262c09b2002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02ab24a9-6918-4a65-ad6a-d42fc8a3897d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132c9443-4d2d-47a1-b878-6262c09b2002",
                    "LayerId": "824273d6-fced-4660-b640-5ee6fe62f7b4"
                }
            ]
        },
        {
            "id": "adacec25-2e74-40bd-82c1-352e0209f6fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2d119ee-ba1c-4f58-a724-3c32e5f0ad31",
            "compositeImage": {
                "id": "d34e0a1c-bda1-44fb-a339-8d27154b06b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adacec25-2e74-40bd-82c1-352e0209f6fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "486ffbcd-23e9-4b18-b9a7-4c36d2f443b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adacec25-2e74-40bd-82c1-352e0209f6fc",
                    "LayerId": "824273d6-fced-4660-b640-5ee6fe62f7b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "824273d6-fced-4660-b640-5ee6fe62f7b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2d119ee-ba1c-4f58-a724-3c32e5f0ad31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 23
}