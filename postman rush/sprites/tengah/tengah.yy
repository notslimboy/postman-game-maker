{
    "id": "3a4eefad-03a3-4f66-a708-1ad3338cccd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tengah",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d3851e6-32d4-41d8-bb7d-937f468c7c94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a4eefad-03a3-4f66-a708-1ad3338cccd9",
            "compositeImage": {
                "id": "fed87e8a-97ed-471a-b3be-f0f99c48041f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d3851e6-32d4-41d8-bb7d-937f468c7c94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34de7b14-f1bf-4534-a025-13328adad5a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d3851e6-32d4-41d8-bb7d-937f468c7c94",
                    "LayerId": "9873b78d-b8d7-4e45-801f-7a884a000bc0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "9873b78d-b8d7-4e45-801f-7a884a000bc0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a4eefad-03a3-4f66-a708-1ad3338cccd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}