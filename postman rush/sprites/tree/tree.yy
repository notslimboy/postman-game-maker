{
    "id": "77550830-c885-4961-ad7d-72577590ff3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 112,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 37,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "467c2c9e-c0eb-41c4-b95c-8bb94cf58aa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77550830-c885-4961-ad7d-72577590ff3d",
            "compositeImage": {
                "id": "488759cf-8291-4e18-a824-bea2fc4dfee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "467c2c9e-c0eb-41c4-b95c-8bb94cf58aa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71aee1d2-78a5-48be-a2c9-a2bb4b5f8684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "467c2c9e-c0eb-41c4-b95c-8bb94cf58aa0",
                    "LayerId": "8c9bb26a-04fb-4b68-8906-3b8906c47d12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "8c9bb26a-04fb-4b68-8906-3b8906c47d12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77550830-c885-4961-ad7d-72577590ff3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 60
}