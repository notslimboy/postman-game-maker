{
    "id": "4bb99aea-0851-41ea-a94c-63a86e4b4128",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33ce5a41-0e3b-49e9-a720-444b1aabcff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bb99aea-0851-41ea-a94c-63a86e4b4128",
            "compositeImage": {
                "id": "1e307cd7-7451-44ef-97fe-7102430d9cd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ce5a41-0e3b-49e9-a720-444b1aabcff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bf3ead1-ed3f-448f-ac9c-c1cbc263fca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ce5a41-0e3b-49e9-a720-444b1aabcff2",
                    "LayerId": "794fc75c-3e1f-4716-aa43-0d4b830ed4ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "794fc75c-3e1f-4716-aa43-0d4b830ed4ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bb99aea-0851-41ea-a94c-63a86e4b4128",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 61,
    "yorig": 130
}