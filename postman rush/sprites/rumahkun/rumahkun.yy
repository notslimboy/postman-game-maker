{
    "id": "26701ebf-3639-4a75-82b3-c98d60a2d2d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rumahkun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 0,
    "bbox_right": 77,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "418275c4-2ec7-4fb4-85c0-285b22f7c48a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26701ebf-3639-4a75-82b3-c98d60a2d2d7",
            "compositeImage": {
                "id": "0fe13df9-af41-4fe3-9af8-a047a1eb861a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "418275c4-2ec7-4fb4-85c0-285b22f7c48a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ef669cf-3655-46ee-a2a9-37a4d7795fde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "418275c4-2ec7-4fb4-85c0-285b22f7c48a",
                    "LayerId": "3d217575-625b-47ad-acaf-ebc9110362f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 94,
    "layers": [
        {
            "id": "3d217575-625b-47ad-acaf-ebc9110362f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26701ebf-3639-4a75-82b3-c98d60a2d2d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 39,
    "yorig": 47
}