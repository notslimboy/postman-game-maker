{
    "id": "82546126-6eff-41d3-b971-bfc0f531403c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "atas",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8575924d-e522-41a5-b2f4-b54f70ecb9fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82546126-6eff-41d3-b971-bfc0f531403c",
            "compositeImage": {
                "id": "3d16ed09-5af3-4714-b7e4-2fd70ff3b159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8575924d-e522-41a5-b2f4-b54f70ecb9fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f38be2d-8195-4054-afac-13b6aa764ffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8575924d-e522-41a5-b2f4-b54f70ecb9fe",
                    "LayerId": "853a0b18-3050-4d69-bfd1-c473805f9617"
                }
            ]
        },
        {
            "id": "d715fd09-9514-4963-ac75-32319bbda1fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82546126-6eff-41d3-b971-bfc0f531403c",
            "compositeImage": {
                "id": "b44de7ee-c2e2-47d1-9096-16d8f2bb4b49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d715fd09-9514-4963-ac75-32319bbda1fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dd0105f-1179-453a-b53f-b12ccd6d1b75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d715fd09-9514-4963-ac75-32319bbda1fd",
                    "LayerId": "853a0b18-3050-4d69-bfd1-c473805f9617"
                }
            ]
        },
        {
            "id": "cb087b0e-f624-45d3-887d-6808010cf883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82546126-6eff-41d3-b971-bfc0f531403c",
            "compositeImage": {
                "id": "67edec90-f9d2-4db1-a10c-3336ffc610c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb087b0e-f624-45d3-887d-6808010cf883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae28b843-29c9-4646-857f-6d9fb3da025f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb087b0e-f624-45d3-887d-6808010cf883",
                    "LayerId": "853a0b18-3050-4d69-bfd1-c473805f9617"
                }
            ]
        },
        {
            "id": "6956c50a-a4be-46e4-a7cf-5f0af22d66af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82546126-6eff-41d3-b971-bfc0f531403c",
            "compositeImage": {
                "id": "b610b649-5cfe-40e3-a7a6-957be628e876",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6956c50a-a4be-46e4-a7cf-5f0af22d66af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c209b5e-bbc3-4ec5-9e09-7cce616cbc79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6956c50a-a4be-46e4-a7cf-5f0af22d66af",
                    "LayerId": "853a0b18-3050-4d69-bfd1-c473805f9617"
                }
            ]
        },
        {
            "id": "e870002f-ef29-4ece-8a50-039133149c43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82546126-6eff-41d3-b971-bfc0f531403c",
            "compositeImage": {
                "id": "4e717248-bc9b-4c1d-8d46-1cf6ece0d35b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e870002f-ef29-4ece-8a50-039133149c43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "778f3712-0e89-44da-9d65-6462daea58cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e870002f-ef29-4ece-8a50-039133149c43",
                    "LayerId": "853a0b18-3050-4d69-bfd1-c473805f9617"
                }
            ]
        },
        {
            "id": "4aa67aa1-bc32-4211-874b-a1e3c782bb99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82546126-6eff-41d3-b971-bfc0f531403c",
            "compositeImage": {
                "id": "ab500aea-64b1-49a7-9125-9b737e319585",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4aa67aa1-bc32-4211-874b-a1e3c782bb99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "483900ec-bf80-4451-874e-327b6b002188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4aa67aa1-bc32-4211-874b-a1e3c782bb99",
                    "LayerId": "853a0b18-3050-4d69-bfd1-c473805f9617"
                }
            ]
        },
        {
            "id": "200716ef-2a29-4d3d-aba3-13e57f30b83b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82546126-6eff-41d3-b971-bfc0f531403c",
            "compositeImage": {
                "id": "57ad555d-fb21-4b1f-9f69-cb84f9a89896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "200716ef-2a29-4d3d-aba3-13e57f30b83b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c69d2c76-2493-4dc0-b5af-b9a0f75e7ffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "200716ef-2a29-4d3d-aba3-13e57f30b83b",
                    "LayerId": "853a0b18-3050-4d69-bfd1-c473805f9617"
                }
            ]
        },
        {
            "id": "3c73012c-6e9a-43c2-80e6-d1103dc98f36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82546126-6eff-41d3-b971-bfc0f531403c",
            "compositeImage": {
                "id": "d5051199-b384-4721-b755-62facc14deb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c73012c-6e9a-43c2-80e6-d1103dc98f36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8adf82a-a6f6-4ac5-8c2d-d2de551fa3a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c73012c-6e9a-43c2-80e6-d1103dc98f36",
                    "LayerId": "853a0b18-3050-4d69-bfd1-c473805f9617"
                }
            ]
        },
        {
            "id": "f7f69de3-7a2f-4cd2-8d71-57feac9fcb72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82546126-6eff-41d3-b971-bfc0f531403c",
            "compositeImage": {
                "id": "3c6d19b7-663f-47b8-9202-0ce5d5df0cd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f69de3-7a2f-4cd2-8d71-57feac9fcb72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f9d7647-ff18-4b4a-a0ad-e753d3573d9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f69de3-7a2f-4cd2-8d71-57feac9fcb72",
                    "LayerId": "853a0b18-3050-4d69-bfd1-c473805f9617"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "853a0b18-3050-4d69-bfd1-c473805f9617",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82546126-6eff-41d3-b971-bfc0f531403c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 35,
    "xorig": 17,
    "yorig": 26
}