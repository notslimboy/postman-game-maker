{
    "id": "fd4450e7-800f-4d85-b6cd-d9883fee4227",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "menara",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 1,
    "bbox_right": 58,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b84b2f8-fe3e-48a1-af26-afaa65e81c3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd4450e7-800f-4d85-b6cd-d9883fee4227",
            "compositeImage": {
                "id": "f4f9b336-62a2-4cd6-9531-410404bec74c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b84b2f8-fe3e-48a1-af26-afaa65e81c3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c6590c4-8f04-46b4-a9ad-1e009b7cb83b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b84b2f8-fe3e-48a1-af26-afaa65e81c3a",
                    "LayerId": "51401b0a-138a-4c8c-8771-1ce37de5bdc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 97,
    "layers": [
        {
            "id": "51401b0a-138a-4c8c-8771-1ce37de5bdc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd4450e7-800f-4d85-b6cd-d9883fee4227",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 59,
    "xorig": 0,
    "yorig": 0
}