{
    "id": "57ec2241-4ed9-4101-b72f-dc475ee43fc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bawah",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d79dd4b2-7dd1-497c-b745-3affed427fb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57ec2241-4ed9-4101-b72f-dc475ee43fc9",
            "compositeImage": {
                "id": "d5b90663-b8f5-4c23-8f33-bc02f47b4a18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d79dd4b2-7dd1-497c-b745-3affed427fb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c10c875-3bc6-4119-8b84-d576329f9f58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d79dd4b2-7dd1-497c-b745-3affed427fb9",
                    "LayerId": "56e0ae99-d605-4098-8575-cd519f2a02dd"
                }
            ]
        },
        {
            "id": "f06207b3-0eae-4c4d-93f4-84b0dfcdd080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57ec2241-4ed9-4101-b72f-dc475ee43fc9",
            "compositeImage": {
                "id": "901deaef-79ff-44fb-a7d5-0a235d8efe30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f06207b3-0eae-4c4d-93f4-84b0dfcdd080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe1c96db-0484-412d-9497-ace846094361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f06207b3-0eae-4c4d-93f4-84b0dfcdd080",
                    "LayerId": "56e0ae99-d605-4098-8575-cd519f2a02dd"
                }
            ]
        },
        {
            "id": "902fdb15-5780-4123-a6b2-b24c3e0861ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57ec2241-4ed9-4101-b72f-dc475ee43fc9",
            "compositeImage": {
                "id": "b83e2d33-872e-4334-bb44-21ec51bcda51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "902fdb15-5780-4123-a6b2-b24c3e0861ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a74e650d-3079-47c8-9cdb-74460971101d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "902fdb15-5780-4123-a6b2-b24c3e0861ba",
                    "LayerId": "56e0ae99-d605-4098-8575-cd519f2a02dd"
                }
            ]
        },
        {
            "id": "18568944-eacc-4c65-9905-23364b360062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57ec2241-4ed9-4101-b72f-dc475ee43fc9",
            "compositeImage": {
                "id": "7f03f3c3-4741-4410-adce-4e94f2ee7d8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18568944-eacc-4c65-9905-23364b360062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be0758a7-fbd8-4ec0-8a42-f30f9ab9fe8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18568944-eacc-4c65-9905-23364b360062",
                    "LayerId": "56e0ae99-d605-4098-8575-cd519f2a02dd"
                }
            ]
        },
        {
            "id": "9576fe31-d9d6-4c69-8572-d9814705a6cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57ec2241-4ed9-4101-b72f-dc475ee43fc9",
            "compositeImage": {
                "id": "70c0b60b-c8d5-472b-bca4-784a8ef0996a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9576fe31-d9d6-4c69-8572-d9814705a6cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02f7a3ef-1022-4b88-a1d8-58246c527892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9576fe31-d9d6-4c69-8572-d9814705a6cf",
                    "LayerId": "56e0ae99-d605-4098-8575-cd519f2a02dd"
                }
            ]
        },
        {
            "id": "42809d65-03b4-419e-8f19-6f856f0596f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57ec2241-4ed9-4101-b72f-dc475ee43fc9",
            "compositeImage": {
                "id": "6dc790b7-ec1b-4db5-8bfc-0d0b08e2d28a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42809d65-03b4-419e-8f19-6f856f0596f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2153481-2542-455c-aea3-3d295f86929f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42809d65-03b4-419e-8f19-6f856f0596f1",
                    "LayerId": "56e0ae99-d605-4098-8575-cd519f2a02dd"
                }
            ]
        },
        {
            "id": "1568f20a-653f-4d88-88e3-a591d1fe3775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57ec2241-4ed9-4101-b72f-dc475ee43fc9",
            "compositeImage": {
                "id": "e7202c8f-35d8-4ce4-a558-065f7195724f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1568f20a-653f-4d88-88e3-a591d1fe3775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d828f5a-ef9a-4821-a039-28a360f39cc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1568f20a-653f-4d88-88e3-a591d1fe3775",
                    "LayerId": "56e0ae99-d605-4098-8575-cd519f2a02dd"
                }
            ]
        },
        {
            "id": "91d326e7-1dd9-4874-a7bd-fce77b5da6c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57ec2241-4ed9-4101-b72f-dc475ee43fc9",
            "compositeImage": {
                "id": "77563254-0b4f-4ff5-a5cb-8a9e55a8ec4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91d326e7-1dd9-4874-a7bd-fce77b5da6c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "535ef959-7893-4d99-9e66-ea878b33828a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91d326e7-1dd9-4874-a7bd-fce77b5da6c5",
                    "LayerId": "56e0ae99-d605-4098-8575-cd519f2a02dd"
                }
            ]
        },
        {
            "id": "46fdb2f6-7b40-478a-9d6e-5bb93526cdef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57ec2241-4ed9-4101-b72f-dc475ee43fc9",
            "compositeImage": {
                "id": "1a9f1d1e-0ab4-4a7e-9d68-e9ee37edaf94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46fdb2f6-7b40-478a-9d6e-5bb93526cdef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49bd480c-cb16-467a-904e-594238badc7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46fdb2f6-7b40-478a-9d6e-5bb93526cdef",
                    "LayerId": "56e0ae99-d605-4098-8575-cd519f2a02dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "56e0ae99-d605-4098-8575-cd519f2a02dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57ec2241-4ed9-4101-b72f-dc475ee43fc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 17,
    "yorig": 26
}