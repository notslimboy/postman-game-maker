{
    "id": "0e1f136b-2147-4df3-b127-13ab9934b86a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc95277b-669e-4ea3-b82e-b9bd62962b8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e1f136b-2147-4df3-b127-13ab9934b86a",
            "compositeImage": {
                "id": "0dc7df84-c2d8-44d2-8f68-694c4051599d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc95277b-669e-4ea3-b82e-b9bd62962b8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e762c67d-dcef-4e8c-b79e-7b1e4c6630ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc95277b-669e-4ea3-b82e-b9bd62962b8c",
                    "LayerId": "22ed600c-b3e3-4b1c-b168-65f2cf708b0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "22ed600c-b3e3-4b1c-b168-65f2cf708b0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e1f136b-2147-4df3-b127-13ab9934b86a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}