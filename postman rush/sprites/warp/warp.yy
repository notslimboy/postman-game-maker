{
    "id": "b67f9c2c-e8d7-41b4-8da9-c3ea1d25f900",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "warp",
    "For3D": false,
    "HTile": true,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cd72817-a835-4683-a369-3f9acf418e30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b67f9c2c-e8d7-41b4-8da9-c3ea1d25f900",
            "compositeImage": {
                "id": "6126fd3a-2100-4f33-ab09-e6d644fdb64c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd72817-a835-4683-a369-3f9acf418e30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a74cb37-a257-4ec3-bc50-254c24248ad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd72817-a835-4683-a369-3f9acf418e30",
                    "LayerId": "380d085f-4464-49ca-aa8d-01c51157138c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "380d085f-4464-49ca-aa8d-01c51157138c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b67f9c2c-e8d7-41b4-8da9-c3ea1d25f900",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": true,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}