{
    "id": "99f7e0ce-c8ec-456c-9934-69cc500ef4e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rumput",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7843b1dc-7d4e-466c-908f-bb14fd71929e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99f7e0ce-c8ec-456c-9934-69cc500ef4e1",
            "compositeImage": {
                "id": "08064c53-8ffa-4b80-8019-f4cf7137f83e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7843b1dc-7d4e-466c-908f-bb14fd71929e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e89db231-ec17-4ed0-be55-3125d30ecd6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7843b1dc-7d4e-466c-908f-bb14fd71929e",
                    "LayerId": "6499d7ed-42d0-4d9e-bc35-043ea0e3ac67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6499d7ed-42d0-4d9e-bc35-043ea0e3ac67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99f7e0ce-c8ec-456c-9934-69cc500ef4e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}