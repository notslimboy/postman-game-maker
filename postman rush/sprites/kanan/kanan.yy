{
    "id": "8868ef1f-9fd8-40d4-9aa7-b24deac36a40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "kanan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffb9419e-97e0-4f47-949a-5af68c64714a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8868ef1f-9fd8-40d4-9aa7-b24deac36a40",
            "compositeImage": {
                "id": "b2bb7616-b39a-4df7-849f-26957b56b9d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb9419e-97e0-4f47-949a-5af68c64714a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01b58d18-5c33-46b9-9109-9b51063add2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb9419e-97e0-4f47-949a-5af68c64714a",
                    "LayerId": "cfcc6f1b-5301-4a2a-8b6e-9fb04ab16170"
                }
            ]
        },
        {
            "id": "e7b9002f-8974-4331-b1b4-8defc78544f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8868ef1f-9fd8-40d4-9aa7-b24deac36a40",
            "compositeImage": {
                "id": "aad7aa86-7f05-46d6-8f34-b38860745104",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7b9002f-8974-4331-b1b4-8defc78544f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c10fa84-8f96-4ffc-b6f9-51a605803823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7b9002f-8974-4331-b1b4-8defc78544f3",
                    "LayerId": "cfcc6f1b-5301-4a2a-8b6e-9fb04ab16170"
                }
            ]
        },
        {
            "id": "2c74f862-82dd-4bf6-8eba-1298009674e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8868ef1f-9fd8-40d4-9aa7-b24deac36a40",
            "compositeImage": {
                "id": "a0fd867c-c1a3-485d-80c9-0fd5150133c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c74f862-82dd-4bf6-8eba-1298009674e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38c972a0-e5af-4f31-8e04-40e676387ceb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c74f862-82dd-4bf6-8eba-1298009674e2",
                    "LayerId": "cfcc6f1b-5301-4a2a-8b6e-9fb04ab16170"
                }
            ]
        },
        {
            "id": "a9a74140-282a-4bb3-a224-b9d1c076d79a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8868ef1f-9fd8-40d4-9aa7-b24deac36a40",
            "compositeImage": {
                "id": "76578f89-d9ce-4d23-b811-0fa904ad4d94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9a74140-282a-4bb3-a224-b9d1c076d79a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4087e311-a077-4f9e-8f45-edc7e7593ac9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9a74140-282a-4bb3-a224-b9d1c076d79a",
                    "LayerId": "cfcc6f1b-5301-4a2a-8b6e-9fb04ab16170"
                }
            ]
        },
        {
            "id": "916e58a5-4d3d-4baf-9fcb-acf286d8c990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8868ef1f-9fd8-40d4-9aa7-b24deac36a40",
            "compositeImage": {
                "id": "50d9dc80-1586-40e4-b099-fdf106f93daa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "916e58a5-4d3d-4baf-9fcb-acf286d8c990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d907914-a97c-4b24-a623-dea4f8809ac6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "916e58a5-4d3d-4baf-9fcb-acf286d8c990",
                    "LayerId": "cfcc6f1b-5301-4a2a-8b6e-9fb04ab16170"
                }
            ]
        },
        {
            "id": "e295baec-193d-47b5-9753-23ed7dc8dad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8868ef1f-9fd8-40d4-9aa7-b24deac36a40",
            "compositeImage": {
                "id": "d1903701-9863-448e-b6df-32a8e6d624a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e295baec-193d-47b5-9753-23ed7dc8dad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a143338e-c65f-49b4-9dec-b582db840106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e295baec-193d-47b5-9753-23ed7dc8dad9",
                    "LayerId": "cfcc6f1b-5301-4a2a-8b6e-9fb04ab16170"
                }
            ]
        },
        {
            "id": "48c07769-e60a-4a39-9eb7-9d20918ef12d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8868ef1f-9fd8-40d4-9aa7-b24deac36a40",
            "compositeImage": {
                "id": "fdf7fe64-98c6-49aa-a9f0-988676a7b5d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48c07769-e60a-4a39-9eb7-9d20918ef12d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69f39a81-11d4-4df7-842b-253aa28c24d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48c07769-e60a-4a39-9eb7-9d20918ef12d",
                    "LayerId": "cfcc6f1b-5301-4a2a-8b6e-9fb04ab16170"
                }
            ]
        },
        {
            "id": "eb998e12-6b5b-4a03-a819-47e4c0a56630",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8868ef1f-9fd8-40d4-9aa7-b24deac36a40",
            "compositeImage": {
                "id": "47454482-4576-4161-97f8-1bdf48565780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb998e12-6b5b-4a03-a819-47e4c0a56630",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2607cd3a-eb8d-4f63-8fc9-9435e85a8691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb998e12-6b5b-4a03-a819-47e4c0a56630",
                    "LayerId": "cfcc6f1b-5301-4a2a-8b6e-9fb04ab16170"
                }
            ]
        },
        {
            "id": "fd12b01a-a347-441b-9898-0e94608b590a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8868ef1f-9fd8-40d4-9aa7-b24deac36a40",
            "compositeImage": {
                "id": "4d2fbb18-8b92-4259-8180-288b2cd0dd95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd12b01a-a347-441b-9898-0e94608b590a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0c98439-fd2e-4ae8-b1aa-3330a6cf8b06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd12b01a-a347-441b-9898-0e94608b590a",
                    "LayerId": "cfcc6f1b-5301-4a2a-8b6e-9fb04ab16170"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "cfcc6f1b-5301-4a2a-8b6e-9fb04ab16170",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8868ef1f-9fd8-40d4-9aa7-b24deac36a40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 26
}