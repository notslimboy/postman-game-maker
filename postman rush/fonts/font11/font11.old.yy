{
    "id": "df330e01-9caf-4b76-ad16-7fb72697553d",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font11",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Neue",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ebf99cf4-823a-4c04-8f0c-c8f5757c78db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ed3d92ec-6fbe-40c6-9d09-312ea0552d2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 37,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f65f3f2a-c578-41ab-94b9-dc2cc2c13824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4ce42558-d949-4321-80ed-27b201849494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 21,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "db5b0477-50a9-45fd-a546-723efa066bfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 13,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e3660093-03f6-409b-96b5-9a23a8d0b31c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f59632dc-c94c-45a1-8110-678cc2060dad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 241,
                "y": 23
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a4f22372-b26f-4453-98ff-587edc95c455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 236,
                "y": 23
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d69284c2-b184-4737-b43b-73b6bc00085e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 230,
                "y": 23
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d6c092cb-aaf4-49b6-ab77-845eb56946e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 224,
                "y": 23
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "94584eb6-5438-471a-8cfc-a5f7e9429f41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 42,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "563a7171-30be-49ec-a183-0a66690364b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 216,
                "y": 23
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7824abed-7f76-48fe-a7a3-dcae219d6734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 203,
                "y": 23
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ebf1f4a1-9341-433d-b664-adfabe8438ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 197,
                "y": 23
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7c5fdb3c-45ad-4dcf-a697-f4baf7bfc4f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 192,
                "y": 23
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "962f40dd-282c-41d9-b933-59305cc08ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 183,
                "y": 23
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7f493a52-dd3c-4867-b3b6-e0928c953a26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 175,
                "y": 23
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "257a866b-a416-4341-9f42-3860fd3fe8e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 169,
                "y": 23
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d65e6121-ba1f-4b14-9546-13d164e9ad2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 161,
                "y": 23
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ab4b0284-cb85-436f-a0ac-4d17c574ca2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 153,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "bf6c3289-a15b-4639-95c1-98b514b5d026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 144,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "45c64012-097b-4787-a8e5-a96f0772a006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 208,
                "y": 23
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a7eed84d-0140-4e7a-81d6-d2475c17e7b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 51,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "66697847-8791-478a-8928-69bea9aeb403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 59,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3a7b7df6-a6bc-4829-82d2-7f8eaebc24a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 67,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3f6d3c3e-9414-4440-9e81-679d12ff7f5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 235,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9d59a51b-6579-4e55-84ef-64e1fd9e9d3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 230,
                "y": 44
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "177cd4bc-bd4b-48f8-bdb0-13b2ec46ec7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 225,
                "y": 44
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "14412ddd-d292-41ee-8963-c02ddb9f95fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 217,
                "y": 44
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "558fc234-b6f1-48c2-907e-b71dae700d68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 209,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7c49df2a-5060-4acb-b819-2240fa0d0ef5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 201,
                "y": 44
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e4bedcb3-32fd-4213-bcba-a93fb18102b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 193,
                "y": 44
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "22355c29-a8e7-42e9-bd1f-9e49c4665d3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 180,
                "y": 44
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "14c8e8ab-6e28-4bdd-96bb-42b81620905c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 171,
                "y": 44
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "519796d4-e052-4076-8d9b-87b1ac1df3a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 162,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fc819552-d99b-450c-88d3-1078c296b3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 154,
                "y": 44
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "085cd628-57d2-4a28-8958-d949fb5cc898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 146,
                "y": 44
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "84eb0324-515a-420c-b0a8-8ece645a3e9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 138,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bd051899-7dff-4182-addd-6d9983993f08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 130,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "953f3bd6-1b0b-402c-accd-860c6e6fcf5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 122,
                "y": 44
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "45929f0d-caaf-40b5-bbf0-88b91dcbe2b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 113,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3e032716-4eec-484b-b23b-df67200bf503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 108,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "74154a9f-8a9f-475a-a47f-693445e4d2a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 102,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "301ec7ae-0baa-4581-a3c7-0d534d3928bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 93,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2cabc349-dd4d-461c-bbe5-187264ef7996",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 85,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0de53894-dbab-4a83-a0de-c8f72075d198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 75,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8788b688-ec6b-432b-a4c8-c0762c5d88fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 135,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2b9b089c-ce84-469b-94be-134cbd5a81f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 127,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3a154a7b-bcb0-41a8-88c4-0dfe42a98357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 119,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f60b2c6c-2797-4b68-bb6e-f22312d6b28a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "584102b4-4e49-47ef-800d-8b4e783b093e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "475941b9-7825-486f-a251-3e389df2114f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "efa81b2d-5de0-43ac-90fd-6888d3a301b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ed5d1e8b-9e72-48fd-a9f8-cdaeaee22908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ea73ea98-f809-4de6-b4b6-387e215001bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4c66f93f-4899-4248-bd02-26712d300b40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "15ffd5fb-a6e9-4762-abe1-b377f4fa6cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5a45b30c-6e90-4e00-8799-386a50e9c527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "508026e9-88f2-47d5-8f62-cb61e6581156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "eeac07e1-8405-47c1-8a62-f010e84fdbb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "04ba5feb-bf74-4b19-ae3d-1fc5da668843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ee55297b-9ba6-4a09-b5a4-6de74d686a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1af10deb-cc8d-41e3-83e2-7762203936c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "91d0d2f6-5340-4192-87e9-e0a9fbc90346",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "89137e67-4b6c-4927-908b-f4dd8bcb35d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5457a68c-3eed-4829-91e6-d48110bc723f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "df56c0cb-9efd-4ddc-b499-d8e326bc6144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "78fd21c0-6a5e-43f3-a0d6-05a1971743d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "edc83220-6b2a-4c34-a3b4-08e3f046d65c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f0bd7ad9-8ff6-41ed-ba11-0c5cf820138f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6676c470-eec9-4f16-8b60-1c2409c243d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "97df1d1b-9af4-419c-81c4-89acfe65f711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e960e3b8-4a97-4a21-bcd3-9e71ed0105b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 18,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9382d921-4756-436e-889b-cecd2925ad6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1d20c7fa-10c6-49c2-ad0b-8dd4547ce1c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 105,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b1f843e8-7d06-47f7-8a95-f0d10263ecfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 96,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3dc7201e-9e0f-419c-aed2-beac8248fa52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 88,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "43d6d42b-494b-484e-902c-c02659057812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 78,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "18964f5a-89e3-4b9e-84f2-2323bb79691e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 69,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c94dcab6-d525-4c1b-8f7d-e4fb6a24fa5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 61,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c8297eb9-95cd-45ea-9709-7eb1d5493cbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 53,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "fbd3a431-ebc9-4c0a-b45c-61617ace422a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 44,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "449e1b7c-caaf-4757-b42c-6cb3b9e7d93b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 35,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c915b61d-89d8-401b-b336-b7ea70412053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 111,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "09a6b794-2547-4f48-a7d3-613e75219736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 27,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bc60582f-01f1-4fbe-a421-e2dc870e0eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "191c47f4-2e2b-4ea4-aeaa-b9bd1e046c5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "809c8e04-dbb0-4a98-a2eb-3174a1f2d81e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a8e76cde-ed91-4f93-96e5-e7c2b803969e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "149a12cf-a6b3-4be4-815e-1a1b2e35254c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cb82500f-b1b2-4ebf-87d4-e02e278086bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a95b163b-355a-458e-a6ea-739901624147",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "10882e81-288d-495f-9ae4-c67812b99c0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b8eda631-0653-4fda-a10a-f3adf308bfa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "691f8552-06ce-4bcb-9b5b-38ca36b930ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 243,
                "y": 44
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "781a85b0-65af-4a2d-9a48-65e9ce178052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 2,
                "y": 65
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}