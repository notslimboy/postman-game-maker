{
    "id": "df330e01-9caf-4b76-ad16-7fb72697553d",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font11",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Neue",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b91537a5-62d3-475f-bd08-1dd63cb26e16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 39,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "be473fd5-5e3b-4c91-8d57-bb6dd1a506ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 39,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 125,
                "y": 125
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "84b1dbd9-0f3d-48d6-8eea-12fdc94500de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 114,
                "y": 125
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "804ef7fc-d9a4-4e33-a255-7a61cc238106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 99,
                "y": 125
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e3003f11-2908-45ad-ab9f-9205a1e3105a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 86,
                "y": 125
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "04a905e4-8e87-4897-b2c7-38dc256e516b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 39,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 67,
                "y": 125
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "910fab8b-c7e5-4f6c-82fb-d2d6add66126",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 53,
                "y": 125
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "15540d31-3965-4240-9980-569c49372e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 39,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 47,
                "y": 125
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "eb7b033d-3398-40a4-86a7-a14096b2f634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 39,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 125
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8300b8f4-5e28-461c-8338-f75125e2de3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 39,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 125
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c4c030f9-fa99-486b-9e2d-3b03a6ca5744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 39,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 132,
                "y": 125
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "22f1526b-f5c0-49ca-bbe2-de430977d323",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 15,
                "y": 125
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a5d7e7e7-b2ed-4739-a9b7-3448c4e21011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 39,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 244,
                "y": 84
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "734f2ca3-8209-4e61-93e5-123d8606af88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 39,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 234,
                "y": 84
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d95bddb3-f83b-48e9-986c-898dabc23145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 39,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 228,
                "y": 84
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c99c0bc6-e34f-48b3-8451-d974ec8280ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 213,
                "y": 84
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c25fc062-f2fb-4d15-a9ef-a25ce601da48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 200,
                "y": 84
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "865a40f9-f26d-4e34-9c49-d607aeea22f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 39,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 190,
                "y": 84
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "dd9a4c1e-582b-4ed1-8c52-4ccb8fd65f2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 177,
                "y": 84
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8bb2ff64-9b43-4b4a-8b3d-422abc8f3230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 164,
                "y": 84
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ca78bbc2-b66b-47a4-a6f6-600b491a98a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 149,
                "y": 84
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b3c92d7b-78a7-48db-88ee-defbf64be3cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 125
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "7db07d5d-dd78-4f97-8357-dad3b4804ec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 148,
                "y": 125
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7b316f9e-08cd-4c6a-9ef5-663bb6ca131d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 161,
                "y": 125
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0398d956-6817-48fa-9a7a-445f75a0b393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 174,
                "y": 125
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "88840146-28b1-4d16-b3da-a80471f98aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 196,
                "y": 166
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4f83bd7c-eacb-4e58-824a-6ab98837bce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 39,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 190,
                "y": 166
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7997dacf-98b6-4a64-8a37-9963e0de14d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 39,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 184,
                "y": 166
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a7316a56-4831-4032-867e-f9d87c23d9aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 171,
                "y": 166
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "970f9d46-ea57-4901-8517-cb1ba49d72c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 158,
                "y": 166
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "05ec3cab-9a44-4b87-b1ff-5caf8d06920a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 145,
                "y": 166
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e97eaea8-75a0-4875-983e-af3aa6db94fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 132,
                "y": 166
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c3d1b599-928f-4814-9220-5cb88443a50e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 39,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 108,
                "y": 166
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "1db113ef-fbf5-46e6-8532-8422ee0e732a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 93,
                "y": 166
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9afa31a7-bff5-4108-bd39-a61dc26d2022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 79,
                "y": 166
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "039814f0-7391-4516-9f02-42f5600e158e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 66,
                "y": 166
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "34d0d92e-5c8b-4903-b117-b2cf2a425879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 53,
                "y": 166
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "084696aa-133e-4d92-a177-b84038581b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 41,
                "y": 166
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "48447a27-81d9-4a14-9f0f-144a4d5ca845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 29,
                "y": 166
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e66b2727-dc59-4dfa-8754-63da341673a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 16,
                "y": 166
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "276f604a-bf87-46f8-a03e-edd98feb3493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 166
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6c2d03ce-ab46-4c10-b781-903f48e9a006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 39,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 241,
                "y": 125
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "168f9056-787c-4772-9412-7f27aae2d28e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 39,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 231,
                "y": 125
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5e5d2cb5-479e-4dad-af02-0665d3f8ecb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 217,
                "y": 125
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f373a43d-44ab-441a-86de-432113498f02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 205,
                "y": 125
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5d743e89-a1b5-499f-9afa-62f1cb4c3144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 39,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 188,
                "y": 125
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7e19e6a6-77ef-4534-ba65-f180cf2e6ccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 135,
                "y": 84
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "8e8ff66d-c618-42b7-a8b2-ccde4255066b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 122,
                "y": 84
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9cbda583-c9ef-4900-9d31-1951082b81e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 109,
                "y": 84
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "092985ad-0e28-45e3-bc68-81b102c05b0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 39,
                "y": 43
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5a0af63c-2471-4a39-89b4-065084f7961c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 43
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "697df870-bde7-4cec-bb5d-6d5beff955e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 43
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "dd3451f4-61a9-4c16-b3e3-832ab6899ec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ff87b825-3c8f-4285-b8dd-4eee990e8fa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "161861ec-987f-48be-9eca-089326cf3d6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f8a94330-f421-4951-815a-67f16717ba14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 39,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "95d42ea0-1f18-4906-bfb1-5eb21f87a9b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ccfb521a-85a9-4119-b465-4305bcf60f35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "24b798aa-db81-42ac-b7e0-54b05b87de07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "bc2835f8-b0ff-46ff-9584-7027ff07cde5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 39,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 30,
                "y": 43
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "db763a0a-fc1b-4cf2-b02d-ad53f8907e7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "768fee3a-f051-4c1f-828b-26caf390c379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 39,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a45780e8-c843-4701-bea8-dccf8ebecdd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "97886142-682d-4868-9a08-4d5e04c2342c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 39,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1c3cf309-792b-46ad-9401-175475a743ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 39,
                "offset": 3,
                "shift": 16,
                "w": 7,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a107df04-c746-41e9-a500-f7ed5404d125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6a9a4dfd-0fbe-444d-8983-732b8b1ae875",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "82504b52-a319-44c1-a3ac-03b106f1878d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fca6c3d2-2de5-4a46-9209-28b91d00ffe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f420e52c-2261-4bfc-9cc0-5179700aa357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e9cf205f-a275-409e-8633-a5763e0ae117",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d09c2c06-cd84-44e1-8a56-33c351ff78f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 53,
                "y": 43
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1cb10056-5c27-4ba4-8803-3d01867276bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 190,
                "y": 43
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "cb64b514-6742-45ca-af6d-2068436170e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 39,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 66,
                "y": 43
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0b864d9b-b96c-4d8c-b8ee-93046a216d00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 39,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 85,
                "y": 84
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "52850d8e-f86e-4237-a46f-1922785876e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 71,
                "y": 84
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7b83dfdf-a2b3-4cda-86f1-471a0c34246d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 39,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 59,
                "y": 84
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "00e79560-e8e7-4e10-bd85-516b6ce12a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 39,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 42,
                "y": 84
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "852b6223-2c07-4598-9622-5220dde4e24c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 39,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 28,
                "y": 84
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7238e781-3ccc-4233-838b-794bfbb7d9fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 15,
                "y": 84
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "74942377-0ec6-4332-a4dd-44cdb9df401a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 84
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4de7fc88-9ff3-40ba-ad53-3466b5c8ee20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 232,
                "y": 43
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "27cf00cc-e067-4a72-bc40-6299444e7edd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 218,
                "y": 43
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3447187c-1e9f-4d41-a5d3-75e6b6c79f0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 95,
                "y": 84
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "cf2caf11-d49c-4fcb-992c-c8ec303ffcfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 204,
                "y": 43
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "151c2ccb-2904-41b8-9634-c3b9cf2c510d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 39,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 177,
                "y": 43
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f548b428-c96f-4d63-850d-f2494b5c8cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 163,
                "y": 43
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3d37af2b-4250-49d5-9e77-9b9a9d630ebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 39,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 143,
                "y": 43
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "74e38b08-9706-483b-a697-71882e78b7ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 128,
                "y": 43
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9524bb8c-b5a9-43cb-8ccf-275e1f8ea0ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 113,
                "y": 43
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4e33076c-084d-4cd5-bad5-e4482610e387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 100,
                "y": 43
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6ed35844-2938-4c78-af28-13e8f39c84e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 39,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 89,
                "y": 43
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9a69045b-ab22-4e20-b2a9-3be083a84656",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 39,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 83,
                "y": 43
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1ab32269-bf34-4564-9e95-414288821cb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 39,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 72,
                "y": 43
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "316060c4-9c21-4ab4-89b6-7bf81f85eff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 39,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 210,
                "y": 166
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b4fd1d57-90c3-4d7e-9d70-6d9c886106b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 39,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 225,
                "y": 166
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}