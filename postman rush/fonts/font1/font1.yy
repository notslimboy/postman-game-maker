{
    "id": "7bb3b95d-81d5-40ac-a630-eea6f8566957",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font1",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Neue",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2378075a-c44f-4699-8eca-5b15e9e995d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d00eb4d2-0733-42e7-976e-7bce1fe054a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 37,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5d5733f9-bc8a-49c4-b2a7-42b31c0f0f48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0df658d8-a0b6-4dc3-a3a9-812f5f7d9f9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 21,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "baaad368-27ab-4d03-be6b-ab39dbb90ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 13,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "924aed0f-6ae7-43ad-9fa8-b291bfe75e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8ae61b6f-08e1-4ca6-8956-bcb81123d24c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 241,
                "y": 23
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f1e93d17-8778-43c1-ae92-435de6fd3444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 236,
                "y": 23
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ed841f50-9272-4c46-a0f5-9fa0cbf80df3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 230,
                "y": 23
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "13812a08-82a3-4470-874a-5f9f3e17334d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 224,
                "y": 23
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3be5bcd5-a630-4853-bb6d-5375c5a89da0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 42,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "81ad9628-6ad6-49b7-87aa-50ec580e28d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 216,
                "y": 23
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "cb3db41e-dd42-4986-954d-e6383cedd01b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 203,
                "y": 23
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "eb753127-3edf-4aca-b4f5-984b9698e11a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 197,
                "y": 23
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d6e8f1e5-fa76-4afa-bf46-2a8119db875b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 192,
                "y": 23
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b58726a1-27e6-42dc-9c22-4d3a08f591d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 183,
                "y": 23
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "46d08489-38da-43ab-8cb2-40eb5ce31eb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 175,
                "y": 23
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5ca2c726-5456-46e2-849c-03283eacbb03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 169,
                "y": 23
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "0d217ceb-d5ce-47d8-98b6-f87e4e30036d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 161,
                "y": 23
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "79981d0b-4c9f-4fcb-b297-c06ec816b342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 153,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f4d7393b-96a5-4703-82cf-9088e5abac31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 144,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "44b470fb-4a16-4256-b476-9b05319f7d59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 208,
                "y": 23
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "34dd0210-973f-4b06-9ede-88c45f1f8659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 51,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d7d7ad3f-acbc-4445-9818-dee996c7f909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 59,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ae932af7-3172-494d-9a68-5cd13e364142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 67,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7d660d85-c3a0-45ec-b8bb-26bf3277a0a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 235,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "24f6972c-9e9f-43f7-a7f3-55108f603647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 230,
                "y": 44
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "634351e3-82b0-43ee-a939-48aefb5c46eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 225,
                "y": 44
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "915a7cdf-0351-477e-92ca-f4b049a59da6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 217,
                "y": 44
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d420c1b3-a61b-40c3-8f5e-1ae02eebbcb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 209,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "288de81f-9c36-4bcd-942d-e4ddde708afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 201,
                "y": 44
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "abf89c51-18fd-434d-a2b4-00547504dac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 193,
                "y": 44
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "84697ebb-b2f1-4a71-a6b0-127f08b21153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 180,
                "y": 44
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "382d5dfb-ec8f-4059-84e7-53d11905a02f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 171,
                "y": 44
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "44623250-62a8-4429-aa60-3347ba9734c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 162,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4cb2d657-7c35-465f-84fd-b7cc4a70df34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 154,
                "y": 44
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "24d11551-0dd5-4a0d-88ec-07a6f24cba2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 146,
                "y": 44
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "04c834e8-6eca-4dc2-92a6-4c36fbf15269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 138,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "efb3a92f-1d6e-49fd-9ea9-88180390cefb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 130,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "630e9fe0-1d82-404d-9416-ed4762ef4a4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 122,
                "y": 44
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b1d52274-6b8e-42fb-9581-d214d08513b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 113,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9500b2ff-062d-4859-8f36-73ffaffda8c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 108,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "77022df3-95ce-4bec-a20d-5497ffd49453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 102,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "924f1740-c0cf-47b1-bbf2-e59dc1e38f0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 93,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "22efeb33-c8e0-46f6-85ab-afe673b65182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 85,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b49329c8-acf9-40d4-bc4d-015235d6f488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 75,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fdcef3ee-465f-4f43-94ed-b6fae565bdf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 135,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "200fab48-0321-4c8a-ac60-087019f56e4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 127,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9a56b419-bab1-4154-83aa-113e78299fbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 119,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c849a3b2-4c36-499f-924b-8b2119108ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "6639533d-bbe3-4933-bead-73de0a5c6da9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6e17578b-02e9-4228-bb23-ce2c86fc6b31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8f5be852-f399-47dc-87a7-3c0ad1869900",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "889e5ae3-f282-44a2-bf6e-59f9c4a9cb20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4a879595-b108-486d-8b80-6ed15e637874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1274e29b-06af-431f-85e3-36011ab64642",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e227b700-6ef3-4182-9538-86e86a8979b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "901ef766-cac9-42ee-aba0-3aadcda558e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8f0f2b78-66b1-4b13-84da-02206191322c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1f210d07-393f-490e-ab2a-3149e06d99bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "cc09d6a0-7b22-4d80-8d32-2285cece61b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "78e24ecc-d1d0-4414-a7c0-cdb0b59be407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "389a96f3-9e1a-42e8-aa24-fbfb27e0e26c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "af28c0cd-1b56-4b53-b316-eda48e38bceb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "39e457e9-518a-48c5-ab5d-fc6bd11d11dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1d7ac1a0-6d7b-4877-b04c-b1a0ca949f77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7a85aaf3-c6bf-45a4-a85e-321640b067ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "60a199ce-0163-4b3d-ae01-e0a37bad40d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "cce19d9a-865c-4931-9788-eeeaa4757d6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fafe98c8-5002-43c6-92b4-03b2b662ada2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "079b6985-835d-4371-869a-40db6a3a84dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f834e934-2cf1-49cb-84ef-6d072eb21688",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ca79467f-4a2a-4e5b-bbec-0b722a789068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 18,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "106d586c-2f2c-433e-bcae-edb75ac8f78c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "041d930f-5670-4232-b261-041b490b5e77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 105,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6bb776ec-ce07-4616-82b2-364e2cb773c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 96,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1eddbc41-119b-4207-94a2-093fefe3d317",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 88,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "45b507bb-d47c-49c1-80c3-29efa763973e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 78,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "09ed2426-fd03-4d59-a380-3f36b9a549c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 69,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f92c4857-be54-4928-af1a-06ba0461ab7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 61,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "db18b336-bff4-4d66-a8b0-b667770a5a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 53,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9ae3d292-e51a-4220-8372-870d51291ee3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 44,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6b76b10f-3d85-49fb-9b33-ab87bf0e8365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 35,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "980cc7a5-0fe2-42e4-b68c-11e99bbb8118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 111,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "1289d280-060f-43d5-90be-de3fb4024f49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 27,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7a8beb83-e575-4002-87d0-a1687a95d6d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ef0dc532-890f-4e48-90bb-d752feebbccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3246ef30-23fc-467d-bcfb-29bab0c9687f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a83ecae6-322e-436c-861c-2bedbb8c335c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e0c3606a-f81b-4d69-a046-9a945827eb50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e7ddce3c-47fa-445c-8352-836f8b8368a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "682e6b17-04a1-45c0-853b-8b4d78bac82a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c5179bbb-9ae1-4209-abd8-9e20704c02fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e594a870-5161-4193-98d3-4588e6abf231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4df33742-c3a2-485b-9fe8-5f9acf0b782d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 243,
                "y": 44
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "875bc78e-7ea3-4729-b5cf-03656fe85614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 2,
                "y": 65
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}