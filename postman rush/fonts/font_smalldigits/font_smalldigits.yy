{
    "id": "61aa6814-b896-4f8c-82e6-78b50876f363",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_smalldigits",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Neue",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "15ab8554-4640-408b-b807-0c7b06f4ebbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0cb19b08-5ef3-41af-b32a-ea0cc120c3ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 101,
                "y": 52
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6541f6e8-3024-49e3-97a7-28f8f81006ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 93,
                "y": 52
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f1c7ca63-823f-4f09-b0c8-d3b51e82dcd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 83,
                "y": 52
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "411b42e4-9751-4d8a-8e05-eaaee608522f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 52
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d7cf9047-790a-4c68-ac02-853f4b2e4174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 61,
                "y": 52
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "456be4f3-a089-47a9-b11a-b6a5815e8071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 51,
                "y": 52
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d0bf0683-414d-4d21-ae83-99d5a8f17514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 46,
                "y": 52
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "68b6cd5b-e5a5-47a3-aa00-588c251fc5dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 39,
                "y": 52
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "aac25ea5-df76-4a5c-8ae6-1884241f704c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 32,
                "y": 52
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "700673df-257a-4d63-8248-0ae1b380d233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 52
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5b723d3c-2409-4b2c-9b6d-54bf28dc4dc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 23,
                "y": 52
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "beb9b84d-738f-457b-99bc-b2312f60f933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 9,
                "y": 52
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a858a1c4-9f56-4569-a021-46b303421144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2d1f3cdd-d221-4dcd-80ba-7c96a408ad7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 249,
                "y": 27
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "62a4d53a-6470-4e93-91c2-514d05c4028f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 239,
                "y": 27
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "93045f05-7ee2-42a8-b694-d057a1bccd39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 230,
                "y": 27
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "517042bd-bcbf-44f8-a8e1-77d0959feabc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 223,
                "y": 27
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c96f61bf-56a1-409d-8844-dcdeaa27b971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 214,
                "y": 27
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "bb41c26d-2bbe-4415-b45e-9308539f04ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 205,
                "y": 27
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "76e0b877-ae4c-4f13-8434-24acfcc0ff9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 195,
                "y": 27
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9a60846c-c3d8-4023-a3a7-d5b7b9e312ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 14,
                "y": 52
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "860494aa-0734-4292-b3ee-462681932a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 116,
                "y": 52
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e9114c53-16e2-467f-808b-421b74b4f6cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 125,
                "y": 52
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3e953dca-e01a-483b-94ad-c52c230b9e09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 134,
                "y": 52
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d10cd87a-d7be-49d3-ad50-7106640d90f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 72,
                "y": 77
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b9077462-5c9d-4eee-b955-c6a893ac52a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 67,
                "y": 77
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "640b9409-de88-4e42-813b-7a17bc81f4a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 62,
                "y": 77
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9aa8e00c-a209-49bb-b15f-d54fd3d02a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 53,
                "y": 77
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f356c43d-4dee-4462-949d-506494457ff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 44,
                "y": 77
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "28318a35-1f38-47fe-a1d8-eecf78ad11c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 36,
                "y": 77
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3834e3fb-b658-408e-ac92-36f3c8b1a167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 77
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "87cc82ee-08aa-4ac8-8a85-2f5e3c924fc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 12,
                "y": 77
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3ccf71fa-df89-4c89-9f04-7cae159ceda4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "403ed549-4098-4d77-9da1-dc57d075c1f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 241,
                "y": 52
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "07f71b02-ccf0-40d4-a6fc-dbbc177853a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 232,
                "y": 52
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "41270dac-2389-4bf7-b872-21fc070f9568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 223,
                "y": 52
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4b4dc0f7-5a0a-4be9-84ec-b2ffb112cdae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 214,
                "y": 52
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "55cc81c2-1377-4bd9-b04f-4a03237e7c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 205,
                "y": 52
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "db13d914-df71-4b7f-9b08-ff6eed5e9720",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 196,
                "y": 52
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5be37ecb-e527-4462-a6fe-62d7b017f29a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 186,
                "y": 52
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3972e8be-3b42-47c4-88cc-fd8895eca961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 181,
                "y": 52
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "93d75285-66c9-47a5-a08c-0f39b3e8f370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 174,
                "y": 52
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e887ed40-ef04-4c5e-9e7f-12da97d9c826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 164,
                "y": 52
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f1304e92-41da-4688-a3ad-8cc792365f3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 155,
                "y": 52
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8e7b3ea1-5333-45df-89a7-c327df3dc963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 143,
                "y": 52
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4c9945fb-db41-47b5-a3b0-500f03c3f74e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 185,
                "y": 27
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7d67074d-91eb-4b5d-96a7-fe2622d56631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 176,
                "y": 27
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0474ab49-b810-48d8-9296-87e48aad647b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 167,
                "y": 27
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "1c922a55-1041-4928-a135-feacf9e6b966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fa5f5951-d250-46a8-92c0-188d88ba22df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2e279966-5476-4350-af48-e712365e5572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "de6baa8a-bed6-4d90-9e54-5afc2fde929b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "20b86d73-3ed8-4e83-980b-697000a4d6a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6dc72a0f-d943-4615-b8bd-fe302a193bfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d7209be4-70b1-4f66-a037-d11a4fa6f64f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f254cc50-232d-4461-bca0-d6d8c58f5c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "cc18e32a-c95e-4710-8e89-68ca460c8d2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6879fe37-ffed-4973-abf2-5b8ce8aa6bce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "854915cd-82c2-4d91-b486-4436d6ab2db9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 23,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "52b5d60a-63fc-464f-ad11-8c47744eb20d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8599fbeb-867c-48c0-99d8-025f8fc70107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "8f386845-c6d2-4612-8990-1ba4564c335d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "cfadf1e2-96f7-47c9-9a38-96695400bf42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ea37a2cd-ef22-4213-8261-e36007a4fd11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 23,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a60f7e29-ba60-4745-958b-942fa3b68d5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f635806b-95c6-418c-bd00-3a394f1b583e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "59cbdbf4-456c-4ccd-bf18-26f80962554f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3e727d07-51c0-43f4-bc69-81b70ba56760",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c8ed4c39-dc6e-400d-8f25-425c3b764f97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "08095ab6-d864-4840-9275-424b52305273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8b1e9f09-6753-4c22-bb5d-93b5402a4f8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d9848dd8-26a1-456a-83fa-54b29e5a2e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 53,
                "y": 27
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9eb6044d-9c97-41b6-8d27-b0c8b51fca0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "85679344-6697-4602-a1a3-4a60196f142d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 151,
                "y": 27
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2292d0ca-6361-48f0-9ae3-a76decf888cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 141,
                "y": 27
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "72ce1df0-1123-4428-8166-656d62ffc15f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 132,
                "y": 27
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b91e14e7-2658-4954-9512-aa516ef7121c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 120,
                "y": 27
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "60439b79-fc8d-4e76-91a4-8581a5f8e4cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 110,
                "y": 27
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c5f4293d-c92b-431d-a840-d3ebb056c567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 101,
                "y": 27
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "389b1f5e-f170-4844-afa0-4f7cad4b3523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 92,
                "y": 27
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "911a5271-bbe3-46bb-a168-7535337df50d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 82,
                "y": 27
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d25916c3-ef2f-4089-a367-dee4abd763e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 72,
                "y": 27
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "255a8433-59a4-4ace-9b8d-546dcc5f09e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 158,
                "y": 27
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "54e3b73b-5a94-4816-bce5-b7ab45265a66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 63,
                "y": 27
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a405f67f-f47b-41e9-aed0-283b8299285c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 44,
                "y": 27
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ba1783ba-2fbe-41c1-a31a-26206a47abac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 35,
                "y": 27
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "560af365-dfa9-43f9-bc6c-28b2765d1225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 22,
                "y": 27
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "dae7aa81-0509-461e-a90b-6e760c55a546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 27
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0bcf2325-87db-4a61-8689-b8a1429e2b3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 2,
                "y": 27
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c99454cb-cccf-4824-9655-e537c7ea0673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "9b49cb52-bbf8-4548-a887-7aa9f4f3f5b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "93330630-bcb2-493e-a064-1d712fe6c3e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 23,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4fd56ba6-4ee9-40b7-95fa-75715783a89a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7e53534a-efc2-42cb-aa62-dd5a948f90d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 81,
                "y": 77
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b153109a-3522-427d-93d1-d0efc3f33468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 23,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 91,
                "y": 77
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}