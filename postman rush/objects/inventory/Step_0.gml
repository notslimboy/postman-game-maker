/// @description Insert description here
// You can write your code in this editor

if (keyboard_check_pressed(ord("E"))) {
	show_inven = !show_inven;}
	
	
if(!show_inven) exit;
mousex = device_mouse_x_to_gui(0);
mousey = device_mouse_y_to_gui(0);

var cell_xbuff = (cell_size + x_buffer) * scale;
var cell_ybuff = (cell_size + y_buffer) * scale;

var i_mousex = mousex - slot_x;
var i_mousey = mousey - slot_y;

var nx = i_mousex div cell_xbuff;
var ny = i_mousey div cell_ybuff;

m_slotx = nx;
m_sloty = ny;

// set selected slot
selected_slots = m_slotx + (m_sloty * inv_slots_width);
