{
    "id": "3bca6406-3c20-49f8-97a6-a80211cd1536",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "inventory",
    "eventList": [
        {
            "id": "8b8875c4-da22-431b-b181-1775b2beaac8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3bca6406-3c20-49f8-97a6-a80211cd1536"
        },
        {
            "id": "82a1a97c-e7c3-4a47-8cae-dc48b2c794f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3bca6406-3c20-49f8-97a6-a80211cd1536"
        },
        {
            "id": "d013067a-3d78-47d3-abbf-73fe0b42e0dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3bca6406-3c20-49f8-97a6-a80211cd1536"
        },
        {
            "id": "dc4cb9f9-968d-42b9-81ae-ea28b9781f75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "3bca6406-3c20-49f8-97a6-a80211cd1536"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}