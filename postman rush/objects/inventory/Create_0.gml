/// @description Insert description here
// You can write your code in this editor

depth = -1;
scale = 2;

show_inven = false; 

inv_slots = 16;
inv_slots_width = 8;
inv_slots_height = 2;

selected_slots = 0;
pickup_items = -1;
m_slotx = 0;
m_sloty = 0;

x_buffer = 2;
y_buffer = 4;

gui_width = display_get_gui_width();
gui_height = display_get_gui_height();

cell_size = 32;

inv_UI_width = 288;
inv_UI_height = 192;

spr_inv_UI = ui_inventory;
spr_inv_items = ui_invetory_item;

spr_item_coloums = sprite_get_width(spr_inv_items)/cell_size;
spr_item_row = sprite_get_height(spr_inv_items)/cell_size;

inv_UI_x = (gui_width * 0.5) - (inv_UI_width * 0.5 * scale);
inv_UI_y = (gui_height * 0.5) - (inv_UI_height * 0.5 * scale);

info_x = inv_UI_x + (5 * scale); 
info_y = inv_UI_y + (5 * scale); 

slot_x = info_x;
slot_y = inv_UI_y + (40 *scale);

// Player information
// 0 = name
ds_player_info = ds_grid_create(2,1);
var info_grid = ds_player_info;
ds_player_info [# 0, 0] = "Name"; 
ds_player_info [# 1, 0] = "Ricardo"; 

// item information
// 0 = none
// 1 = mail

ds_inventory = ds_grid_create (2 , inv_slots);

enum item {
	none	= 0,
	mail	= 1,
	heigh	= 2,
	
}



var yy = 0; repeat(inv_slots){
	ds_inventory [# 0, yy] = irandom_range(1, item.heigh-1);
	ds_inventory [# 1, yy] = irandom_range(1,10); 
	yy += 1;
}