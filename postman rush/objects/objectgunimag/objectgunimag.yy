{
    "id": "08f2fcfb-8367-4aad-8e23-53cd8fe1b1de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objectgunimag",
    "eventList": [
        {
            "id": "488a46a3-7a6a-432e-bc7b-c8042783f4af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "08f2fcfb-8367-4aad-8e23-53cd8fe1b1de"
        },
        {
            "id": "d0f53099-20bb-4cee-b46d-ed9754fdfb19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "08f2fcfb-8367-4aad-8e23-53cd8fe1b1de"
        },
        {
            "id": "f1cab5ed-4f71-46e2-983f-4fd066c0e7cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "219662ce-23bf-4911-a685-52c33b21a933",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "08f2fcfb-8367-4aad-8e23-53cd8fe1b1de"
        },
        {
            "id": "12351a6c-2226-478f-8547-efb89e306aa0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2fe9d73c-cac6-4b53-aea7-cd8e076642fb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "08f2fcfb-8367-4aad-8e23-53cd8fe1b1de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "008ea04f-4141-4c6b-b98c-530ee979480c",
    "visible": true
}