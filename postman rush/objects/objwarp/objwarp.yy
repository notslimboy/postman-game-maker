{
    "id": "2fe9d73c-cac6-4b53-aea7-cd8e076642fb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objwarp",
    "eventList": [
        {
            "id": "0b0506c4-49d3-40a8-9004-f7763a9a184f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "08f2fcfb-8367-4aad-8e23-53cd8fe1b1de",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2fe9d73c-cac6-4b53-aea7-cd8e076642fb"
        },
        {
            "id": "e34db617-5acf-4a54-828c-acb565f31a05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2fe9d73c-cac6-4b53-aea7-cd8e076642fb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b67f9c2c-e8d7-41b4-8da9-c3ea1d25f900",
    "visible": true
}