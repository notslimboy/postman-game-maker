/// @description Insert description here
// You can write your code in this editor

image_speed = 0; 
walkSpeed = 1;
collisionSpeed = walkSpeed + 2;

active_slot = 0; 
current_item = 0;
equip_slot[0] = "";
equip_slot[1] = "";


if (mouse_wheel_up()) {
    if (active_slot > 0) {
        active_slot -= 1;
    } else {
        active_slot = 1;
    }
} else if (mouse_wheel_down()) {
    if (active_slot < 1) {
        active_slot += 1;
    } else {
        active_slot = 0;
    }
}