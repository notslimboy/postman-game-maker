{
    "id": "786233c4-843a-41df-9e87-9b61093399cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "post",
    "eventList": [
        {
            "id": "4267b432-4306-4aa3-9e2a-ec3c7609f9b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "786233c4-843a-41df-9e87-9b61093399cd"
        },
        {
            "id": "32de802a-6763-4a93-b3ec-97a3001761fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "786233c4-843a-41df-9e87-9b61093399cd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "57ec2241-4ed9-4101-b72f-dc475ee43fc9",
    "visible": true
}