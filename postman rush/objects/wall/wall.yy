{
    "id": "219662ce-23bf-4911-a685-52c33b21a933",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "wall",
    "eventList": [
        {
            "id": "dce79302-5434-45f9-847d-82f92d912a0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "08f2fcfb-8367-4aad-8e23-53cd8fe1b1de",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "219662ce-23bf-4911-a685-52c33b21a933"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "a36f24e1-ca15-41a8-b7da-f8d8a1188c4c",
    "visible": true
}