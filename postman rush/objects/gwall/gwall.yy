{
    "id": "281881c8-9af1-45bd-9524-da154ddae6de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "gwall",
    "eventList": [
        
    ],
    "maskSpriteId": "5539e672-3722-421d-94ed-6dc9afba962d",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5539e672-3722-421d-94ed-6dc9afba962d",
    "visible": true
}