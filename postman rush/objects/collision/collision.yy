{
    "id": "4ffa7c11-10c1-44f0-b474-87a2423b9a24",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "collision",
    "eventList": [
        {
            "id": "ac7ef821-4a51-449e-a8e5-0122372f7fe6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "786233c4-843a-41df-9e87-9b61093399cd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4ffa7c11-10c1-44f0-b474-87a2423b9a24"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "99f7e0ce-c8ec-456c-9934-69cc500ef4e1",
    "visible": false
}